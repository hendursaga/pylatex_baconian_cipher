# pylatex_baconian_cipher

Helper script to generate typeset poetry with a hidden layer of messaging, using the [Baconian cipher](https://en.wikipedia.org/wiki/Bacon's_cipher).
