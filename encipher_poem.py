#!/usr/bin/env python

from pylatex.base_classes import CommandBase
from pylatex.package import Package
from pylatex import Command, Document, UnsafeCommand
from sys import argv, exit
from yaml import safe_load


CODES = {
    'a': "AAAAA", 'b': "AAAAB", 'c': "AAABA", 'd': "AAABB",
    'e': "AABAA", 'f': "AABAB", 'g': "AABBA", 'h': "AABBB",
    'i': "ABAAA", 'j': 'ABAAA', 'k': "ABAAB", 'l': "ABABA",
    'm': "ABABB", 'n': "ABBAA", 'o': "ABBAB", 'p': "ABBBA",
    'q': "ABBBB", 'r': "BAAAA", 's': "BAAAB", 't': "BAABA",
    'u': "BAABB", 'v': 'BAABB', 'w': "BABAA", 'x': "BABAB",
    'y': 'BABBA', 'z': 'BABBB'
}


class AttribCommand(CommandBase):
    _latex_name = 'attrib'


def encipher_text(doc, text, secret):
    text_index = 0

    for char in secret:
        try:
            encoded = CODES[char.lower()]
        except KeyError:
            continue

        for index in range(0, 5):
            while True:
                prev_char = None
                text_char = text[text_index + index]

                if text_char.lower() in CODES:
                    break
                else:
                    if prev_char == "\n" and text_char == "\n":
                        doc.append('\\!')  # new stanza
                    elif text_char == ' ':
                        doc.append(Command('hspace', '1 mm'))
                    else:
                        doc.append(text_char)

                    prev_char = text_char
                    text_index += 1  # TODO: bounds check

            if encoded[index] == 'A':
                command = Command('textsf', text_char)
            else:
                command = Command('textrm', text_char)

            doc.append(command)

        text_index += 5

    # doc.append(Command('textrm', text[text_index:]))
    doc.append(text[text_index:])


if len(argv) != 3:
    print('Usage: ' + argv[0] + " <poem file> <secret string>")

    exit(1)

yaml = safe_load(open(argv[1]))

doc = Document()
doc.preamble.append(Package('verse'))
attrib_cmd = UnsafeCommand('newcommand', r'\attrib', options=1,
                           extra_arguments=r'\nopagebreak{\raggedleft\footnotesize #1\par}')

doc.append(attrib_cmd)
doc.append(Command('begin', arguments='verse'))
doc.append(Command('poemtitle', yaml['title']))

# TODO: check lengths

encipher_text(doc, yaml['text'], argv[2])

doc.append(Command('end', arguments='verse'))

doc.append(AttribCommand(arguments=yaml['author']))
doc.generate_pdf('out', clean_tex=False)
